# Resetting TP-Link Powerline plugs

A few years ago, I bought a pair of TP-Link Powerline plugs, and I've been paying for that mistake ever since (the Devolo ones have given me zero trouble). They constantly desynchronize and need to be paired again.

This is the process, so I don't forget:

* Press the button for around 7 seconds, until the lights all go dark and light up again. Don't do this while the pairing light is blinking, it doesn't seem to work.
* Press the pairing button once.
* Repeat this process on the second plug.
* They should pair now.
* Get Devolo plugs.

* * *

<p style="font-size:80%; font-style: italic">
Last updated on May 25, 2023. For any questions/feedback,
email me at <a href="mailto:hi@stavros.io">hi@stavros.io</a>.
</p>
