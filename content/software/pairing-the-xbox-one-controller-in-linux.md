# Pairing the Xbox One controller in Linux

Sometimes, the Xbox One controller won't pair in Linux, instead connecting and disconnecting rapidly. To fix that:

* Install [xpadneo](https://github.com/atar-axis/xpadneo).
* If the controller keeps connecting and disconnecting when you turn it on, make sure you upgrade to the latest firmware, that fixes that issue.
* Pair!

That should be all that's required, your controller should now be paired.

* * *

<p style="font-size:80%; font-style: italic">
Last updated on July 29, 2023. For any questions/feedback,
email me at <a href="mailto:hi@stavros.io">hi@stavros.io</a>.
</p>
