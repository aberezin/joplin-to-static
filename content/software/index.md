# Contents

Click on a link in the list below to go to that page:

1. [Black pills](../../software/black-pills.html)
1. [Cloning cards/fobs with a Proxmark3](../../software/cloning-cards-fobs-with-a-proxmark3.html)
1. [Getting VoWiFi working on Xiaomi.eu](../../software/getting-vowifi-working-on-xiaomi-eu.html)
1. [Monero GUI syncing stuck with Ledger](../../software/monero-gui-syncing-stuck-with-ledger.html)
1. [Pairing the Xbox One controller in Linux](../../software/pairing-the-xbox-one-controller-in-linux.html)
1. [Resetting TP-Link Powerline plugs](../../software/resetting-tp-link-powerline-plugs.html)
1. [Test and format SD cards](../../software/test-and-format-sd-cards.html)
