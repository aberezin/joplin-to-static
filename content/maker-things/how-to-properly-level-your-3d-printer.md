# How to properly level your 3D printer

I see many people on our 3D printer Facebook group ask about adhesion issues with their printer. 99% of the time, this is because they have leveled improperly, and not because of the bed material.

I have taken photos of filament when the nozzle was leveled at various heights from the bed. I greatly recommend [an adjustable Z endstop](https://www.thingiverse.com/thing:1370547), which will save you lots of time when leveling/tramming.

In this first photo, I have leveled too high (meaning the nozzle is too high compared to the bed). You can see that the filament is cylindrical, which means that it has just dropped onto the bed (or minimally touched it), leading to very poor adhesion. You can imagine that, if the filament is just dropped onto the bed, it won’t adhere well:

![too-high.jpg](/resources/576a1fedf9f144a69fe262866e9f268a.jpg)

The next photo is also a bit too high. It’s not as high as the previous photo, so it has partially adhered, but there are gaps between the rows and adhesion still won’t be great:

![too-high-2.jpg](/resources/57cca138661644a99f3e9422665cbbb5.jpg)

The next photo has been leveled too low. The filament is mushed against the bed, but it’s mushed too much, leading to transparent-looking rows of filament. The specific filament I’m using tends to look transparent even when leveled properly, but this is too low regardless:

![too-low.jpg](/resources/1aac14963f294f74b1288a02e24eaead.jpg)

For the last photo, I’ve leveled it pretty much perfectly. You can see that the rows aren’t cylindrical at all, instead they’re long strips that are touching each other properly and don’t look too transparent. If you insert a piece of paper between the bed and the nozzle, you will be able to move it with some difficulty, but it won’t move very freely.

![just-perfect.jpg](/resources/d95ef0cfd696407b82734616ca963205.jpg)

There you have it! If your filament looks cylindrical, follow your printer’s manual to reduce the distance between bed and nozzle. If it looks transparent and missing in places completely, you need to increase the distance. If it looks mushed and has the proper color, and the rows are touching each other just so, you’re perfect and ready to print!

I hope this guide has helped you, feel free to share it with your friends.

* * *

<p style="font-size:80%; font-style: italic">
Last updated on November 20, 2020. For any questions/feedback,
email me at <a href="mailto:hi@stavros.io">hi@stavros.io</a>.
</p>
