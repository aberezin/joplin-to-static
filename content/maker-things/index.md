# Contents

Click on a link in the list below to go to that page:

1. [Battery discharge curves](../../maker-things/battery-discharge-curves.html)
1. [Details about my Sapphire Pro](../../maker-things/details-about-my-sapphire-pro.html)
1. [Electronics tips](../../maker-things/electronics-tips.html)
1. [GRBL_ESP32 tips](../../maker-things/grbl-esp32-tips.html)
1. [How to properly level your 3D printer](../../maker-things/how-to-properly-level-your-3d-printer.html)
1. [Info on various hardware components](../../maker-things/info-on-various-hardware-components.html)
1. [Installing BLTouch-compatible firmware onto the TwoTrees Sapphire Pro](../../maker-things/installing-bltouch-compatible-firmware-onto-the-twotrees-sapphire-pro.html)
1. [Notes on my TimSav](../../maker-things/notes-on-my-timsav.html)
