# Transportable C1 Chaser

I have a [C1 Chaser](https://www.banggood.com/C1-Chaser-1200mm-Wingspan-EPO-Flying-Wing-FPV-Racer-Aircraft-RC-Airplane-KIT-p-1102080.html?custlinkid=667416&p=6W16207412782201611V), and it's a fantastic wing.
It flies great, and is very efficient. The only problem I had with it was that it's too long to easily carry around, as it has a 1.2m wingspan.

I thought that a C1 Chaser that can break apart for easy transport would be the ideal long-range wing, so I bought a second one and made some modifications to it.
I'm listing the modifications here so you can do the same if you want to.


## Spar

The biggest issue is making the spar removable.
The best way I've found to do that is to use IKEA drinking straws, they have an internal diameter of around 7.5mm, making them ideal for putting the spar through.
If you find the spar has too much jiggle, I've found that applying some CA glue around it makes it thick enough to fit snugly in the straw.

I've cut three straws to length, sanded the straws and the spar channel a little with coarse sandpaper, and glued the former in the latter, as you can see here (the straw in the photo is not cut to length yet):

[![](/resources/5ac6bff04962497083b8403ed0fded98.jpg)](/resources/5ac6bff04962497083b8403ed0fded98.jpg)


[![](/resources/65ff483180ad483795b32d0b65f72613.jpg)](/resources/65ff483180ad483795b32d0b65f72613.jpg)

Make sure to leave a few mm from the inside edge of the wing (so the straw doesn't touch the edge, again not pictured in the photo), so the spar's edge doesn't rub against the straw, to avoid splitting it.
Also, leave the spar in the straws while gluing, so the straws don't lose their shape (but make sure you don't get glue on the spar and end up gluing it to the straw).

For glue, I use [E6000](https://www.banggood.com/ZHANLIDA-152550ml-E6000-B6000-Adhesive-Transparent-Glue-for-RC-Models-p-1604315.html?cur_warehouse=CN&ID=62834216283418&rmmds=search&custlinkid=667416&p=6W16207412782201611V) (sold as Goop in the US) for pretty much everything, but for this one you can use your favorite glue that you know won't dissolve EPO.

This is what the final result will look like:

[![](/resources/6c9034ecf9d9414d87fbc6b4ec2fa28e.jpg)](/resources/6c9034ecf9d9414d87fbc6b4ec2fa28e.jpg)


## Wings

Connecting the wings has two problems:

First, you need the wing to be stable against rotation around the spar.
Already, the notch the C1 has stops the wing from rotating up, but it does not stop it from rotating down (since you're meant to glue it in place).

Second, you obviously need the wing to be stable against left/right motion, or it will slide off the spar.
To solve these problems, I [designed a simple connector with two halves](https://cad.onshape.com/documents/bd7ed4d575e32e06d46234b6/w/ab2adb2ae56e261604342410/e/ad8c70c01b2f8865eb84f5c6), one of which goes onto the fuselage and one onto the wing:

[![](/resources/dd331e351cd0492794619526ccb89a3f.jpg)](/resources/dd331e351cd0492794619526ccb89a3f.jpg)

The connector has two components, one is a cylindrical channel for a carbon tube (which you can glue to one of the pieces, as in the photo) that stops the wing from rotating around the spar, and the other is a channel for an o-ring, which keeps the two halves from moving left to right.

I used a 6mm outer diameter carbon tube and an o-ring with an inner diameter of 28mm and a cross-section diameter of 3.5mm. A slightly thicker and slightly longer o-ring will also probably work.

To install, place the connector over the underside of the wing and mark the outline, then cut the foam and glue the connector in.
Do the same with the other side, into the fuselage, as shown in the photo below.
To make sure the connectors are glued straight, I recommend cutting the foam to shape, placing the connectors in on both sides, and sliding them into each other (using the tube), making sure to leave some distance and watch for any places where glue has leaked, to prevent the wing and the fuselage from gluing together.
Also, make sure to not use glue the part of the connector that's not going to be next to foam, on the fuselage side, to prevent the wing tab from adhering when you slide them together (watch out for glue seeping from under the connector towards the tab).

This is the end result:

[![](/resources/b765e8d8e3db4eceaad1f33862fb7bcc.jpg)](/resources/b765e8d8e3db4eceaad1f33862fb7bcc.jpg)

To mount the wings, just insert the spar into the fuselage, insert the wings (making sure both tubes go into their respective holes), and place the o-ring into both halves of the connector.
That gives you a secure and quick assembly.


## Vertical stabilizers

The last part is securing the vertical stabilizers. I did this with [two very small 3D-printed pieces](https://cad.onshape.com/documents/2c189f2c6522261154d2323e/w/a7e91c32873cec3a9110e900/e/7b35e4b324d972fd5c8a49f6), I slot the stabilizers horizontally (on the left/right axis) onto the fuselage and then connect the wings, which keeps the stabilizers securely in place.

First, press the stabilizer-side part (the Π-shaped one) onto the stabilizer on the place where you want it (I put it as shown in the photo), and cut slightly *inside* the indentation it creates.
Then, glue it into place:

[![](/resources/9e4279dbc32a4c28886b8dbb5e308f86.jpg)](/resources/9e4279dbc32a4c28886b8dbb5e308f86.jpg)

Then, insert the fuselage-side part (the inverted-T-shaped one) into the stabilizer-side part, align the stabilizer with the fuselage and press it in, to create the indentation onto the fuselage EPO, so you know where to cut.
Insert the part into the cut and glue it in:

[![](/resources/ac25d2c2031441549f266d83ed6ac030.jpg)](/resources/ac25d2c2031441549f266d83ed6ac030.jpg)

When everything has set, you can install the stabilizers by simply slotting the two parts into each other and mounting the wing.
That way, the stabilizer isn't going anywhere, and you can install and remove it very quickly.


## The end

You're done! Enjoy your disassemblable, transportable C1 Chaser!

* * *

<p style="font-size:80%; font-style: italic">
Last updated on February 03, 2021. For any questions/feedback,
email me at <a href="mailto:hi@stavros.io">hi@stavros.io</a>.
</p>
