# E6000 hinges

I love Goop/E6000 glue, it's very versatile and makes for a great bond.
However, do remember to work with it outside, as it is not safe to breathe.
One of the coolest things you can do with it is make hinges, for control surfaces as well as for any hatches you may have.

E6000 hinges are very strong (I tried tearing one by hand but couldn't, in the end I had to cut it), and very flexible (you can easily turn the hinged surface 180 degrees up or down with very little force).
They also have almost zero play, making them ideal for control surfaces.

Here's how:

- [ ] Make sure your surfaces are clean and straight.
      Since you're going to make a hinge by placing a very thin layer of glue over the two pieces, the pieces need to actually be straight and contacting on the part where the glue is going to go.
	  If there's a recess or groove where your elevons meet, this won't work very well because your glue layer will be too thick to bend properly.
- [ ] Apply some masking tape parallel to the hinge line, 1 cm above and 1cm below it, so you don't get glue elsewhere on the wing.
- [ ] Place the elevon/aileron/other control surface in position and apply some masking tape to the two ends, to hold the elevon in place.
      Take care to position it straight on both axes, the control surface should be at the same level as the wing surface across its length *and* the control surface shouldn't be deflected up or down.
- [ ] Apply a thin layer of glue (around 1mm thin) on the top of the joint (not on both sides), and spread it evenly with a credit card or piece of paper.
- [ ] Wait for it to dry, and you're done!

Because a description might be hard to follow, you can see the process in more detail [in this video](https://www.youtube.com/watch?v=ar9ttyGs1rM).

* * *

<p style="font-size:80%; font-style: italic">
Last updated on March 22, 2021. For any questions/feedback,
email me at <a href="mailto:hi@stavros.io">hi@stavros.io</a>.
</p>
