# Omnibus F4 V3 pinout

This is the pinout of the Omnibus F4 V3:

[![53b3161d509dcc7bbfb43c89b16b0bae.png](/resources/99f5c91454204c1d9740a8d9b876833b.png)](/resources/99f5c91454204c1d9740a8d9b876833b.png)

Also:

![omnibus-f4-v5-pinout2.jpg](/resources/410f3ce004c64fe9af68bfd6856d3e53.jpg)



* * *

<p style="font-size:80%; font-style: italic">
Last updated on January 06, 2021. For any questions/feedback,
email me at <a href="mailto:hi@stavros.io">hi@stavros.io</a>.
</p>
