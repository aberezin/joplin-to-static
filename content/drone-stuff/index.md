# Contents

Click on a link in the list below to go to that page:

1. [A simple guide to PID control](../../drone-stuff/a-simple-guide-to-pid-control.html)
1. [E6000 hinges](../../drone-stuff/e6000-hinges.html)
1. [FPV frequency chart](../../drone-stuff/fpv-frequency-chart.html)
1. [Fixing the Matek ELRS receivers](../../drone-stuff/fixing-the-matek-elrs-receivers.html)
1. [Getting uninverted SBUS on a no-name FrSky-compatible receiver](../../drone-stuff/getting-uninverted-sbus-on-a-no-name-frsky-compatible-receiver.html)
1. [Getting uninverted SBUS/SmartPort on the FrSky XSR receiver](../../drone-stuff/getting-uninverted-sbus-smartport-on-the-frsky-xsr-receiver.html)
1. [INAV tuning tips](../../drone-stuff/inav-tuning-tips.html)
1. [Miscellaneous](../../drone-stuff/miscellaneous.html)
1. [Omnibus F4 V3 pinout](../../drone-stuff/omnibus-f4-v3-pinout.html)
1. [Omnibus F4 pro servo diode](../../drone-stuff/omnibus-f4-pro-servo-diode.html)
1. [QGroundControl to Mission Planner conversion script](../../drone-stuff/qgroundcontrol-to-mission-planner-conversion-script.html)
1. [Transmitter external module pinout](../../drone-stuff/transmitter-external-module-pinout.html)
1. [Transportable C1 Chaser](../../drone-stuff/transportable-c1-chaser.html)
