# Interesting projects

This is a list of interesting projects that I don't want to forget about, in no particular order:

## AI stuff

* [aider](https://github.com/paul-gauthier/aider): Pair-program with GPT-4 in your terminal.
* [Sweep](https://sweep.dev/): Makes pull requests from GitHub issues.

## Misc

* [VHS](https://github.com/charmbracelet/vhs): Generates gifs of cli programs from pre-written scripts (great for documentation).

* * *

<p style="font-size:80%; font-style: italic">
Last updated on August 07, 2023. For any questions/feedback,
email me at <a href="mailto:hi@stavros.io">hi@stavros.io</a>.
</p>
