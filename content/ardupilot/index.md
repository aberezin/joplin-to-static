# Contents

Click on a link in the list below to go to that page:

1. [ArduPilot recommended settings](../../ardupilot/ardupilot-recommended-settings.html)
1. [ArduPilot setup checklist](../../ardupilot/ardupilot-setup-checklist.html)
1. [Bitmask calculator](../../ardupilot/bitmask-calculator.html)
1. [Building ArduPilot](../../ardupilot/building-ardupilot.html)
1. [Configuring a switch as a relay](../../ardupilot/configuring-a-switch-as-a-relay.html)
1. [Current sensor calibrator](../../ardupilot/current-sensor-calibrator.html)
1. [DJI FPV configuration](../../ardupilot/dji-fpv-configuration.html)
1. [ELRS preferred configuration](../../ardupilot/elrs-preferred-configuration.html)
1. [HD OSD tool](../../ardupilot/hd-osd-tool.html)
1. [Installing WTFOS on DJI](../../ardupilot/installing-wtfos-on-dji.html)
1. [Miscellaneous notes](../../ardupilot/miscellaneous-notes.html)
1. [Reverse thrust](../../ardupilot/reverse-thrust.html)
1. [Statistics calculator](../../ardupilot/statistics-calculator.html)
1. [TECS tuning calculator](../../ardupilot/tecs-tuning-calculator.html)
1. [Transfer config between craft](../../ardupilot/transfer-config-between-craft.html)
1. [Tuning the TECS](../../ardupilot/tuning-the-tecs.html)
